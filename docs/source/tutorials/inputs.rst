.. code-block::
    Engine engine = new Engine();
    engine.Run();
    code . . .

=========================================================
Inputs in MaidLib
=========================================================

To make UI buttons or player movement on WASD or Arrow keys on your keyboard, MaidLib has simple methods to check if the button or key is pressed and other input methods.

Mouse inputs
------------
**Methods**



``Mouse`` is a class you can get every mouse input information from. In this class:

- ``Mouse.GetMouseDown(MouseButton.LEFT)`` - returns true if left mouse button is clicked or false if not.

- ``Mouse.GetMouseUp(MouseButton.MoseButton.LEFT)`` - returns true if left mouse button is up after click or false if not.

- ``Vector2 Position`` - returns position in the application window space

- ``bool isButtonDown`` - checks if mouse (whatever button) is clicked

- ``bool isButtonUp`` - checks if mouse (whatever button) is up after a click

``MouseButton`` is an enum that contains every mouse button:

- ``MouseButton.LEFT`` - Left mouse button

- ``MouseButton.RIGHT``- Right mouse button

- ``MouseButton.MIDDLE`` - Middle mouse button



