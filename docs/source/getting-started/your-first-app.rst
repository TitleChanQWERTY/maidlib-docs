Your First App
=====================

Let's create a project ♥
------------------------------------------

First, you need to **download** the version of MaidLib you need. You need to create a C# project in which you 
will use MaidLib. Next, **copy** the version of MaidLib you 
downloaded earlier **into the folder of this project**. For all platforms, the main folder will be called **maidLib.**

You can also simply download the maidLib folder from the `GitLab repository <https://gitlab.com/TitleChanQWERTY/maidlib-cs>`_. 

**For example:**

.. code-block::

    ProjectName:
        maidLib
        -- your file--

Now **open your .csproj and add the path to maidLib.csproj there**. 
Also, add a folder where you will have your resources (Images, Audio, Fonts, etc.)

**For Example:**

.. code-block::

    <ItemGroup>
        <ProjectReference Include="../../maidLib/maidLib.csproj" />
    </ItemGroup>

    <ItemGroup>
        <Content Include="assets\**">
        <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>
        </Content>
    </ItemGroup>

Now, **open your main script file (for example, program.cs), 
and import a namespace called maidLib**

.. code-block:: csharp

    using maidLib;

Next, at the main launch point (i.e., Main), 
**create an instance of the Engine class, and then run Engine.Run()**

.. code-block:: csharp

    using maidLib;

    public static void Main()
    {
        Engine engine = new Engine();
        engine.Run();
    }

Next, just run your program 
(you can simply write dotnet run in the console)

Most likely, you will see the following:

.. image:: your-first-window.png

If so, congratulations, you have made your first window. 
On the first startup, you'll be initialized with all the necessary 
things, including **ProjectSetings**.
