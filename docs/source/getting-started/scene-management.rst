Scene Management
=================

Let's create scenes that we can put GameObjects on in the future. 

Scene is a parent class that you can inherit. 
It has all the ready-made features and fun :)

How create new Scene?
---------------------

In order to add a Scene to the list, you need to execute 
``Scene.AddScene()``, in the parameters of which you need to specify 
the scene you want to add.

**For Example:**

.. code-block:: csharp

    Scene.AddScene(new Scene());

If you write ``new Scene()`` to the parameters, 
an instance of the parent class Scene will be created.

To create your own scene, create a class that will inherit 
from the parent Scene class. 

**For Example:**

.. code-block:: csharp

    class Scene1 : Scene
    {
    
    }

Then you can simply create an instance of 
your scene and add it to the list.

**For Example:**

.. code-block:: csharp

    using maidLib;

    class Program
    {
       public static void Main()
       {
           Engine engine = new Engine();
           Scene.AddScene(new Scene1());
           engine.Run();
       }
    }

    class Scene : Scene
    {

    }

Congratulations! You have created your own stage! 
But that's not all...

Scene Functions
---------------

The Scene has many functions:

- ``AddGameObject()`` - **Add a GameObject to the Scene**
- ``DestroyGameObject()`` - **Remove the GameObject from the Scene**
- ``InitScene()`` - **Your scene initiation. Here you will most often write AddGameObject()**
- ``Start()`` - **Called after InitScene(). It can be used when you need to do something later than in InitScene()**
- ``Update()`` - **Your function to update Scene. Called before updating the GameObject**
- ``GetScene()`` - **It helps to simply get the scene by index**
- ``ChangeScene()`` - **It helps to change the scene. You can specify an index, or an instance of the Scene class**
- ``AddScene()`` - **Add a scene to the list**

You will use all these functions depending on your task. 

Change Background Color
-----------------------

If you want to change the background color of a scene, 
you can simply change the ColorBackground in the scene. 
To do this, you need to specify the new color you want to create. 

**For Example:**

.. code-block:: csharp

    ColorBackground = new Color(0, 250, 0); // We will get a green background color

We recommend writing this in ``InitScene()``
