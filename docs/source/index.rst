MaidLib - Docs
==============

Welcome to the official and free documentation for `MaidLib <https://titlechanqwerty.gitlab.io/maidlib/>`_, 
the free and open source 2D tool for developing your own games 
and applications. If you are new in MaidLib, we recommend that you 
read the :ref:`introduction page <doc_about_intro>` to get a general idea of what MaidLib is and how it works.

About this Docs
---------------

This documentation is designed to make it easy to learn, use, 
and distribute MaidLib. We try to fill the documentation with 
all the materials, tell you about the subtleties, and not 
leave you alone ♥


.. toctree::
    :hidden:
    :maxdepth: 1
    :caption: About
    :name: sec-general

    about/introduction
    about/how-does-it-work

.. toctree::
    :hidden:
    :maxdepth: 1
    :caption: Getting Started
    :name: sec-general

    getting-started/your-first-app
    getting-started/scene-management

.. toctree::
    :hidden:
    :maxdepth: 1
    :caption: Tutorials
    :name: sec-general

    tutorials/inputs
