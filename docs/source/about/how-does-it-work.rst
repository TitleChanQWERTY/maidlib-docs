How does it work?
=================

MaidLib uses the concepts of OOP and ECS 
(Entity Component System) to simplify all this. 

Let's imagine the situation. You need to receive a package, for example, 
a picture. You order this package online. In order to get this package, 
it goes through a lot of checking and shipping processes, and finally, 
the courier brings it to you. That's roughly how MaidLib works.

Now let's analyze everything in more detail!

GameComponent
-------------
The stuff that makes up the package. What you can fill it with. 
A GameComponent is every little element of a big package.

GameObject
----------
This is the package you want to get. But let's imagine that you are also given the opportunity to customize this 
package for yourself. For example, change the color of your T-shirt, or choose a different process for your laptop. 
This is an example of how GameObject work

Scene
-----
This is the vehicle that will deliver the package to the department. 
They will fully process, check, and send the package to you.

Engine
------
This is the courier who will deliver your package. 
His goal is to deliver the package to the recipient correctly. 
Also, while he delivers your package, he handles a lot of things. 
Which package is yours, which way to choose to deliver the package, 
what music to play while he's driving, and so on.


MaidLib works on a similar principle. 
Your task is just to make up, order the package, choose the department, 
and receive the package. Easy, isn't it?
