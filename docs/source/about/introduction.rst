.. _doc_about_intro:

Introduction
=============

Welcome to the MaidLib community ♥. 
This documentation was created to explain MaidLib and help newcomers understand 
how to make their own games and applications using MaidLib.

.. code-block:: csharp

    Engine engine = new Engine();
    engine.Run();


About MaidLib
-------------
MaidLib is a tool (but not an engine) for developing your own games and applications, 
limited only by your imagination. MaidLib provides a simple, intuitive, and functional 
development toolkit. The purpose of the tool is to free the hands of developers and give 
them the opportunity to create.

Introducing GameObject
-----------------------
These are objects that MaidLib can understand. They can have their own functionality, 
they can be simple managers, and entities at the same time!


Introducing GameComponent
--------------------------
This is a system that allows you to write a large amount of functionality 
and provide it to a large number of GameObjects. Instead of writing each 
functionality of the steering wheel in each GameObject, you can write your 
own GameComponent, which can then be reused in many other GameObjects.

Introducing Scene
-------------------------
This is a system that allows you to store, process, retrieve, 
and play with GameObjects. Scenes are very important for your project to work. 
They are a container that stores another container (GameObject).
